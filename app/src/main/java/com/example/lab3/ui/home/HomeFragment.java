package com.example.lab3.ui.home;

import android.graphics.ColorSpace;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.lab3.ItemAdapter;
import com.example.lab3.R;
import com.example.lab3.databinding.FragmentHomeBinding;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private FragmentHomeBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                new ViewModelProvider(this).get(HomeViewModel.class);

        binding = FragmentHomeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        final TextView textView = binding.textHome;
        homeViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }
    RecyclerView recyclerView;
    List<ColorSpace.Model> itemList;

    public View View() {
        super.onDestroyView();
        binding = null;
        Inflater inflater;
        View view=inflater.inflate(R.layout.fragment_new, container, false);

        recyclerView=view.findViewById(R.id.recycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        //initData();

        recyclerView.setAdapter(new ItemAdapter(initData(),getContext()));



        return view;
    }

    private List<ColorSpace.Model> initData() {

        itemList=new ArrayList<>();
        final boolean add = itemList.add(new ColorSpace.Model(R.drawable.thumbnail, "video 1"));
        itemList.add(new ColorSpace.Model(R.drawable.thumbnail,"video 1"));
        itemList.add(new ColorSpace.Model(R.drawable.thumbnail,"video 1"));
        itemList.add(new ColorSpace.Model(R.drawable.thumbnail,"video 1"));
        itemList.add(new ColorSpace.Model(R.drawable.thumbnail,"video 1"));
        itemList.add(new ColorSpace.Model(R.drawable.thumbnail,"video 1"));
        itemList.add(new ColorSpace.Model(R.drawable.thumbnail,"video 1"));
        itemList.add(new ColorSpace.Model(R.drawable.thumbnail,"video 1"));
        itemList.add(new ColorSpace.Model(R.drawable.thumbnail,"video 1"));

        return itemList;
    }
}